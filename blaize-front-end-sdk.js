(function (define) {
  define(function (require, exports) {

    function parseHeaders(rawHeaders) {
      var headers = {};
      // Replace instances of \r\n and \n followed by at least one space or horizontal tab with a space
      // https://tools.ietf.org/html/rfc7230#section-3.2
      var preProcessedHeaders = rawHeaders.replace(/\r?\n[\t ]+/g, ' ');
      preProcessedHeaders.split(/\r?\n/).forEach(function (line) {
        var parts = line.split(':');
        var key = parts.shift().trim();
        if (key) {
          var value = parts.join(':').trim();
          if (!headers[key]) headers[key] = [];
          headers[key].push(value);
        }
      });
      return headers;
    }

    function httpRequest(method, path, body, headers, callback) {
      var request = new XMLHttpRequest();

      request.onload = function () {
        callback(null, {
          status: request.status,
          headers: parseHeaders(request.getAllResponseHeaders() || ''),
          body: request.responseText
        });
      };

      request.onerror = request.ontimeout = request.onabort = function () {
        callback('HTTP ' + method + ' request to ' + path + ' failed.')
      }

      request.withCredentials = true;
      request.open(method, path, true);

      if (!headers['Accept']) headers['Accept'] = 'application/json';
      for (var name in headers) {
        if (!headers.hasOwnProperty(name)) continue;
        request.setRequestHeader(name, headers[name]);
      }

      request.send(['get', 'delete', 'head', 'options'].includes(method.toLowerCase()) ? null : body);
    }

    function httpGet(path, callback) {

      httpRequest('GET', path, null, {}, callback);
    }

    function httpGetJson(path, callback) {

      httpRequest('GET', path, null, {}, function (error, response) {
        response.body = JSON.parse(response.body);
        callback(error, response);
      });
    }

    function httpPostJson(path, body, callback) {

      httpRequest('POST', path, JSON.stringify(body), { 'Content-Type': 'application/json' }, function (error, response) {
        response.body = JSON.parse(response.body);
        callback(error, response);
      });
    }

    function getAnonymousSession() {

      var callback, token, fingerprint;

      if (arguments.length == 1 && arguments[0] instanceof Function) {
        callback = arguments[0];
      } else if (arguments.length == 3 && arguments[2] instanceof Function) {
        token = arguments[0];
        fingerprint = arguments[1];
        callback = arguments[2];
      } else {
        console.error('Blaize SDK: getAnonymousSession requires a callback function.\nUsage:\n\tBlaizeSDK.getAnonymousSession(function(error, success) {})\n\tor\n\tBlaizeSDK.getAnonymousSession(token, fingerprint, callback)');
        return;
      }

      var path = '/blaize/anonymous-session';
      if (token && fingerprint) path += '?token=' + token + '&fingerprint=' + fingerprint;

      httpRequest('POST', path, null, {}, function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 201) {
          callback(result.body.message)
        } else {
          callback(null, 'Anonymous session created; a cookie has been set.');
        }
      });
    }

    function register(json, callback) {

      if (arguments.length != 2 || (!(arguments[1] instanceof Function))) {
        console.error('Blaize SDK: register requires a UserModel and callback function.\n Usage\n\tlet body = {\n\t\tidentifiers: { email_address: "person@domain.tld" },\n\t\tvalidators: { password: "p@55w0rd" },\n\t\tattributes: { x: "y" }\n\t};\n\tBlaizeSDK.register(body, function(error, result) {};');
        return;
      }

      httpPostJson('/blaize/register', json, function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          callback(null, 'User registered; a cookie has been set');
        }
      });
    }

    function login(json, callback) {

      if (arguments.length != 2 || (!(arguments[1] instanceof Function))) {
        console.error('Blaize SDK: login requires a CoreUserDetails and callback function.\n Usage\n\tlet body = {\n\t\tidentifiers: { email_address: "person@domain.tld" },\n\t\tvalidators: { password: "p@55w0rd" }\n\t};\n\tBlaizeSDK.login(body, function(error, result) {};');
        return;
      }

      httpPostJson('/blaize/login', json, function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          callback(null, 'User logged in; a cookie has been set');
        }
      });
    }

    function logout(callback) {

      if (arguments.length != 1 || (!(arguments[0] instanceof Function))) {
        console.error('Blaize SDK: logout requires a callback function.\n Usage\n\tBlaizeSDK.logout(function(error, result) {};');
        return;
      }

      httpRequest('POST', '/blaize/logout', null, {}, function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          document.cookie = 'blaize_session= ; expires = Thu, 01 Jan 1970 00:00:00 GMT';
          callback(null, 'User logged out; the blaize_session cookie has been removed');
        }
      });
    }

    function forgetMe(callback) {

      if (arguments.length != 1 || (!(arguments[0] instanceof Function))) {
        console.error('Blaize SDK: forgetMe requires a callback function.\n Usage\n\tBlaizeSDK.forgetMe(function(error, result) {};');
        return;
      }

      httpRequest('POST', '/blaize/forget-me', null, {}, function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          document.cookie = 'blaize_session= ; expires = Thu, 01 Jan 1970 00:00:00 GMT';
          callback(null, 'User deleted; the blaize_session cookie has been removed');
        }
      });
    }

    function entitlementChallenge(entitlements, callback) {

      if (arguments.length != 2 || !(entitlements instanceof Array) || !(arguments[1] instanceof Function)) {
        console.error('Blaize SDK: entitlementChallenge requires an array of entitlement IDs and callback function.\n Usage\n\tBlaizeSDK.entitlementChallenge(["00000000-0000-0000-0000-000000000000"], function(error, result) {};');
        return;
      }

      var requestBody = {
        contentIdentifier: window.location.pathname,
        entitlementIds: entitlements
      };

      httpPostJson('/blaize/authorization/challenge', requestBody, function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          callback(null, result.body);
        }

      });
    }

    function accessDecision(options, callback) {

      if (arguments.length != 2 || !(arguments[1] instanceof Function)) {
        console.error('Blaize SDK: accessDecision requires a decision request and callback function. See API docs for full details of the request.\n Usage\n\tlet body = {\n\trequest_headers: { }\n\t};\n\tBlaizeSDK.accessDecision(body, function(error, result) {};');
        return;
      }

      if (!options.path) options.path = window.location.pathname;

      httpPostJson('/blaize/decision-engine', options, callback);
    }

    function getAccount(callback) {

      if (arguments.length != 1 || (!(arguments[0] instanceof Function))) {
        console.error('Blaize SDK: getAccount requires a callback function.\n Usage\n\tBlaizeSDK.getAccount(function(error, result) {};');
        return;
      }

      httpGetJson('/blaize/account', function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          callback(null, result.body);
        }
      })
    }

    function getProfile(callback) {

      if (arguments.length != 1 || (!(arguments[0] instanceof Function))) {
        console.error('Blaize SDK: getProfile requires a callback function.\n Usage\n\tBlaizeSDK.getProfile(function(error, result) {};');
        return;
      }

      httpGetJson('/blaize/profile', function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          callback(null, result.body);
        }
      })
    }

    function updateProfile(profile, merge, callback) {

      if (arguments.length != 3 || typeof arguments[1] !== 'boolean' || !(arguments[2] instanceof Function)) {
        console.error('Blaize SDK: updateProfile requires a new profile (or part of one), a boolean indicating if the new profile should be merged with the old one (otherwise it will replace it) and a callback function.\n Usage\n\tBlaizeSDK.updateProfile({"x": "not y"}, true, function(error, result) {};');
        return;
      }

      var method = (merge) ? 'PATCH' : 'PUT';
      httpRequest(method, '/blaize/profile', JSON.stringify(profile), { 'Content-Type': 'application/json' }, function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          callback(null, 'Profile updated');
        }
      });
    }

    function getExtendedProfileDocument(documentId, callback) {

      if (arguments.length != 2 || (!(arguments[1] instanceof Function))) {
        console.error('Blaize SDK: getExtendedProfileDocument requires a document ID and a callback function.\n Usage\n\tBlaizeSDK.getExtendedProfileDocument("savedThings", function(error, result) {};');
        return;
      }

      httpGetJson('/blaize/profile/' + documentId, function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          callback(null, result.body);
        }
      });
    }

    function updateExtendedProfileDocument(documentId, documentContent, callback) {

      if (arguments.length != 3 || !(arguments[2] instanceof Function)) {
        console.error('Blaize SDK: updateExtendedProfileDocument requires a document ID, a new profile and a callback function.\n Usage\n\tBlaizeSDK.updateExtendedProfileDocument("savedThing", {"x": "not y"}, function(error, result) {};');
        return;
      }

      httpRequest('PUT', '/blaize/profile/' + documentId, JSON.stringify(documentContent), { 'Content-Type': 'application/json' }, function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          callback(null, 'Extended profile document updated');
        }
      });
    }

    function listSubscriptions(provider, callback) {

      if (arguments.length != 2 || !(arguments[1] instanceof Function)) {
        console.error('Blaize SDK: listSubscriptions requires a callback function.\n Usage\n\tBlaizeSDK.listSubscriptions(function(error, result) {};');
        return;
      } else if (provider != 'braintree') {
        console.error('Blaize SDK: list subscriptions supports the following payment providers:\n\t* braintree');
        return;
      }

      httpGetJson('/blaize/payment/' + provider + '/subscriptions', function (error, result) {
        if (error) {
          callback(error);
        } else if (result.status != 200) {
          callback(result.body.message);
        } else {
          callback(null, result.body);
        }
      });
    }

    exports.getAnonymousSession = getAnonymousSession;
    exports.register = register;
    exports.login = login;
    exports.logout = logout;
    exports.forgetMe = forgetMe;
    exports.entitlementChallenge = entitlementChallenge;
    exports.accessDecision = accessDecision;
    exports.getAccount = getAccount;
    exports.getProfile = getProfile;
    exports.updateProfile = updateProfile;
    exports.getExtendedProfileDocument = getExtendedProfileDocument;
    exports.updateExtendedProfileDocument = updateExtendedProfileDocument;
    exports.listSubscriptions = listSubscriptions;
  });
}(typeof define === 'function' && define.amd ? define : function (factory) {
  if (typeof exports !== 'undefined') {
    //commonjs
    factory(null, exports);
  } else {
    factory(null, (window['BlaizeSDK'] = {}));
  }
}));
